# GDB Usage

[Refer to this document for using GDB.](http://webhotel4.ruc.dk/~keld/teaching/CAN_e14/Readings/gdb.pdf) [^1] <br>
The files used in this guide are _not provided_, but I've provided a file in the repo to test with. Feel free to mess with the "char.S" file using GDB as you follow along. You can download the code onto your machine and copy it onto the server using the scp command. In terminal, go into the directory you have the file saved and run this command, replacing hostname with your own. <br>
``scp char.S hostname:.``

## GDB Examples

### Running the program
In order to make use of GDB, you must compile your program with the "-g" flag. The "-g" flag will produce debugging information that GDB can work with. <br> 
You can run your program in GDB using `r[un]`. This will execute the program.<br>
![Run program](/assets/running.gif) <br>
In the above gif, notice that upon running GDB, the last line will read "Reading symbols from [program name]... done."
If you forget to compile with the "-g" flag, GDB will not have any symbols to reference to when debugging and you will see this message. <br>
<img src="assets/nosymbols.png" width="550"> <br>
You can also run your program with command line arguments in GDB. <br>
<img src="assets/runarguments.png" width="500"> <br>
Executing the program through GDB is extremely useful for finding segmentation faults (SIGSEGV) <br>
![SIGSIEV](/assets/sigsegv.gif) <br> 
In this example, we can see that our program encounters a segmentation fault at line 44 in factorial.S. <br>
<img src="assets/SIGSEGVimg.png"> <br>

### Breakpoint at a label/line number
We can set a breakpoint at a label or a line number in a specified file. Upon program execution, the program will run until the breakpoint,
but it will not execute the instruction at the breakpoint. <br>
Once we reach a breakpoint, we can execute instructions one-by-one using `s[tep]` and `n[ext]`. The difference between `step` and `next` is that `next` will not step into a subroutine call. <br>
![Breakpoint](/assets/breakstepnext.gif) <br>
*Note: pressing enter without a command will repeat the last command.* <br>
In the above, `next` was used to run `bl printf` without stepping into the `printf` function. Try using `step` at `bl printf` and see where it takes you.
``c[ontinue]`` is used to continue execution until the program hits another breakpoint or until the end of the program. <br>
You can delete all of your breakpoints with `d[elete]`. <br>
<img src="assets/deletebreak.png" width="500"> <br>

### Show all or one register
In this example, we can see some value is being loaded into the register x0. After running this instruction, we can check what is inside that register using the `p[rint]` command. In addition, we can see all of our registers using `i[nfo] r[egister]`. <br>
![Print and info](/assets/printandinfo.gif)
### Examine the contents of a memory address
You can examine the contents of a specified memory address using `x/[count][format] [address]`. This example uses `x/8xg` and `x/8dg`. <br>
The `8` means that it will display 8 words.
`x` means that it will display the words in hexadecimal,
and `d` means decimal. If you wanted to see it in binary, you would use `t`, so maybe you would use `x/8tg`.
Lastly, the `g` is the size modifier--this will display giant words.
All together, ``x/8xg`` will examine the contents of a memory address and display 8 giant words in hexadecimal. <br>
![Memory](assets/seememory.gif) <br>
*Note: ``p[rint]`` can also use the formats ``/x, /d, /t`` etc...* <br>
In this example, x0 has a pointer (64-bits) at the base address, and two words after it. Using giant words is great in this case, since we can see the whole address. Using that address, we can then examine it. <br>
We can also see the two words after it--they are 0x131 and 0x131.
### Complete list of formats and size modifiers
Formats
<ul>
    <li>o - octal</li>
    <li>x - hexadecimal</li>
    <li>d - decimal</li>
    <li>u - unsigned decimal</li>
    <li>t - binary</li>
    <li>f - floating point</li>
    <li>a - address</li>
    <li>c - char</li>
    <li>s - string</li>
    <li>i - instruction</li>
</ul>
Size modifiers
<ul>
    <li>b - byte</li>
    <li>h - halfword (2 bytes/16-bit value)</li>
    <li>w - word (4 bytes/32-bit value)</li>
    <li>g - giant word (64-bit value)</li>
</ul>

### Extra resources
[GEF (GDB Enhanced Features)](https://github.com/hugsy/gef) - Great GDB plugin with lots of features (very useful for seeing memory content and your code all on one screen).

## Source
[^1]: Roskilde University
